from exceptions import (BadDatetimeLineException,
                        IntervalEndHasAttainedException,
                        NotRegisteredSubscriberException,
                        NotActivatedSubscriberException,
                        BadDescriptionLineException,
                        NotRegisteredRateException,
                        NotRegisteredAdditionalPacketException,
                        BadDecimalNumberException)

from log_parser import LogParser
from tests.support import (get_log_parser_with_two_different_subscribers,
                           get_lite_add_packet, get_hard_rate, get_lite_rate)
from sub import Sub
from used_units import UsedUnits

import os
import time
import pytest
from decimal import Decimal


test_path = os.path.split(__file__)[0]


# handle_log_file
def test_handle_log_file():
    lp = get_log_parser_with_two_different_subscribers()

    add_pack = get_lite_add_packet()
    lp.add_packets[add_pack.name] = add_pack

    lp.handle_log_file(os.path.join(test_path, 'good_log'))


# handle_line
@pytest.mark.parametrize('lines', [
    ['day 15.12.1916',
     'change_rate 23434 lite_rate 0 12:12:12',
     'change_rate 23433 lite_rate 0 12:12:13',
     'add_money 23434 200 12:12:14',
     'add_money 23433 200 12:12:15',
     'connect_additional_service 23434 useless 12:12:16',
     'call 23434 23433 12:12:17 12:14:13',
     'inet 2343434 4 5 13:17:58',
     'disconnect_additional_service 23434 useless 12:12:16',
     'sms 23434 23433 12:12:17 12:14:13',
     'change_rate 23433 hard_rate 100 12:15:13',
     'deactivate_subscriber 23433 12:15:14',
     'add_money 23433 200 12:15:15',
     'activate_subscriber 23433 12:15:16',
     'day 16.12.2216']
])
def test_handle_line_good(lines):
    lp = get_log_parser_with_two_different_subscribers()

    add_pack = get_lite_add_packet()
    lp.add_packets[add_pack.name] = add_pack

    for line in lines:
        lp.handle_line(line)


# get_detailed_line
@pytest.mark.parametrize('detailing_line, result', [
    ('inet 2343434 4 5 13:17:58', 'Интернет от 2343434\n'
                                  'Кол-во входящего трафика: 4\n'
                                  'Кол-во исходящего трафика: 5\n'
                                  'Время: 13:17:58\n'),
    ('day 21.12.2016', 'День 21.12.2016'),
    ('call 2343434 2343433 13:16:59 13:17:58',
     'Звонок от 2343434 к 2343433 начался в 13:16:59, закончился в 13:17:58'),
    ('sms 2343434 2343433 13:16:59 13:17:58',
     'Смс от 2343434 к 2343433 отправлена в 13:16:59, принята в 13:17:58'),

    ('change_rate 2343434 hard 234 12:12:12',
     '2343434 сменил тариф на hard. Стоимость операции: 234. Время: 12:12:12'),
    ('add_money 2343434 201 12:12:12',
     '2343434 пополнил счет на сумму 201 в 12:12:12'),
    ('activate_subscriber 2343434 12:12:12', '2343434 активирован в 12:12:12'),
    ('deactivate_subscriber 2343434 12:12:12',
     '2343434 заблокирован в 12:12:12'),
    ('connect_additional_service 2343434 lite 12:25:35',
     '2343434 подключил дополнительную услугу lite в 12:25:35'),
    ('disconnect_additional_service 2343434 lite 12:25:35',
     '2343434 отключил дополнительную услугу lite в 12:25:35')
])
def test_get_detailed_line_all_needs_detail(detailing_line, result):
    lp = get_log_parser_with_two_different_subscribers()
    lp.day_command('21.08.2016')

    # Приоритет у all_needs_detail больше,
    # чем detailing_numbers.
    lp.detailing_numbers = ['2343433']
    lp.need_all_detail = True

    assert lp.get_detailed_line(detailing_line) == result


@pytest.mark.parametrize('detailing_line, result', [
    ('day 21.12.2016', 'День 21.12.2016'),
    ('call 2343434 2343433 13:16:59 13:17:58',
     'Звонок от 2343434 к 2343433 начался в 13:16:59, закончился в 13:17:58'),
    ('sms 2343434 2343433 13:16:59 13:17:58',
     'Смс от 2343434 к 2343433 отправлена в 13:16:59, принята в 13:17:58'),
    ('inet 2343433 4 5 13:17:58', 'Интернет от 2343433\n'
                                  'Кол-во входящего трафика: 4\n'
                                  'Кол-во исходящего трафика: 5\n'
                                  'Время: 13:17:58\n'),
    ('add_money 2343433 201 12:12:12',
     '2343433 пополнил счет на сумму 201 в 12:12:12'),
    ('disconnect_additional_service 2343433 lite 12:25:35',
     '2343433 отключил дополнительную услугу lite в 12:25:35'),
    # None, потому что 2343434 не назначен как детализируемый.
    ('change_rate 2343434 hard 234 12:12:12', None),
    ('activate_subscriber 2343434 12:12:12', None),
    ('deactivate_subscriber 2343434 12:12:12', None),
    ('connect_additional_service 2343434 lite 12:25:35', None)
])
def test_get_detailed_line_detailing_number(detailing_line, result):
    lp = get_log_parser_with_two_different_subscribers()
    lp.day_command('21.08.2016')
    lp.detailing_numbers = ['2343433']

    assert lp.get_detailed_line(detailing_line) == result


@pytest.mark.parametrize('detailing_line', [
    'day 21.12.2016',
    'call 2343434 2343433 13:16:59 13:17:58',
    'sms 2343434 2343433 13:16:59 13:17:58',
    'inet 2343434 4 5 13:17:58',
    'change_rate 2343434 hard 234 12:12:12',
    'add_money 2343434 201 12:12:12',
    'activate_subscriber 2343434 12:12:12',
    'deactivate_subscriber 2343434 12:12:12',
    'connect_additional_service 2343434 lite 12:25:35',
    'disconnect_additional_service 2343434 lite 12:25:35'
])
def test_get_detailed_line_equivalently_none(detailing_line):
    lp = get_log_parser_with_two_different_subscribers()

    assert lp.get_detailed_line(detailing_line) is None


# day_command
def test_day_command_wrong_date_line():
    lp = get_log_parser_with_two_different_subscribers()

    # Устанавливаем текущий день, чтобы исключить
    # неоднозначность дальнейшей проверки метода.
    lp._current_day = lp.date_line_to_datetime_obj('21.08.2016')
    assert lp._current_day == lp.date_line_to_datetime_obj('21.08.2016')

    # Проверяем работу метода на некорректной строке.
    with pytest.raises(BadDatetimeLineException):
        lp.day_command('21-08-2016')

    # Проверяем текущий день в обработчике лога после
    # некорректной смены текущего дня.
    assert lp._current_day is None


def test_day_command_date_changed():
    lp = get_log_parser_with_two_different_subscribers()

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('30.08.2016')

    # Проверяем баланс
    assert lp._current_day == lp.date_line_to_datetime_obj('30.08.2016')


def test_day_command_add_packets():
    lp = get_log_parser_with_two_different_subscribers()

    sub1 = lp.subs.get('2343434')
    sub1.connect_add_packet(get_lite_add_packet())

    sub2 = lp.subs.get('2343433')

    assert sub1.balance == Decimal('-2')
    assert sub2.balance == Decimal('0')

    # Меняем текущий день в LogParser.
    lp.day_command('30.08.2016')

    # Проверяем баланс
    assert sub1.balance == Decimal('-4')
    assert sub2.balance == Decimal('0')


def test_day_command_after_rate_fee_passed():
    lp = get_log_parser_with_two_different_subscribers()

    sub1 = lp.subs.get('2343434')
    sub2 = lp.subs.get('2343433')

    sub1.after_rate_pay_passed = 29
    sub2.after_rate_pay_passed = 13

    assert sub1.after_rate_pay_passed == 29
    assert sub2.after_rate_pay_passed == 13

    assert sub1.rate.pay_period == 30
    assert sub2.rate.pay_period == 30

    # Меняем текущий день в LogParser.
    lp.day_command('30.08.2016')

    # Проверяем баланс
    assert sub1.after_rate_pay_passed == 0
    assert sub2.after_rate_pay_passed == 14


def test_day_command_pay_rate():
    lp = get_log_parser_with_two_different_subscribers()

    sub1 = lp.subs.get('2343434')
    sub2 = lp.subs.get('2343433')

    sub1.after_rate_pay_passed = 29
    sub2.after_rate_pay_passed = 13

    assert sub1.balance == Decimal('0')
    assert sub2.balance == Decimal('0')

    assert sub1.rate.pay_period == 30
    assert sub2.rate.pay_period == 30

    assert sub1.rate.subscription_fee == Decimal('12')

    # Меняем текущий день в LogParser.
    lp.day_command('30.08.2016')

    # Проверяем баланс
    assert sub1.balance == Decimal('-12')
    assert sub2.balance == Decimal('0')


def test_day_command_start_day_more_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.start_day = lp.date_line_to_datetime_obj('30.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('29.08.2016')

    assert lp._current_day is None


def test_day_command_start_day_equals_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.start_day = lp.date_line_to_datetime_obj('29.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('29.08.2016')

    assert lp._current_day == lp.date_line_to_datetime_obj('29.08.2016')


def test_day_command_start_day_less_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.start_day = lp.date_line_to_datetime_obj('29.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('30.08.2016')

    assert lp._current_day == lp.date_line_to_datetime_obj('30.08.2016')


def test_day_command_end_day_more_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.end_day = lp.date_line_to_datetime_obj('30.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('29.08.2016')

    assert lp._current_day == lp.date_line_to_datetime_obj('29.08.2016')


def test_day_command_end_day_equals_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.end_day = lp.date_line_to_datetime_obj('29.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    lp.day_command('29.08.2016')

    assert lp._current_day == lp.date_line_to_datetime_obj('29.08.2016')


def test_day_command_end_day_less_current_day():
    lp = get_log_parser_with_two_different_subscribers()
    lp.end_day = lp.date_line_to_datetime_obj('29.08.2016')

    assert lp._current_day is None

    # Меняем текущий день в LogParser.
    with pytest.raises(IntervalEndHasAttainedException):
        lp.day_command('30.08.2016')


# call_command
def test_call_command_not_four_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.call_command('7 7 lite')


def test_call_command_incorrect_number():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.call_command('one 1 lite lite')

    with pytest.raises(NotRegisteredSubscriberException):
        lp.call_command('1 one lite lite')


def test_call_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    with pytest.raises(NotActivatedSubscriberException):
        lp.call_command('2343434 2343433 12:12:12 12:14:12')


def test_call_command_incorrect_time():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.call_command('2343434 2343433 12-25-34 12-25-35')


def test_call_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()

    out_sub = lp.subs.get('2343434')
    in_sub = lp.subs.get('2343433')

    assert out_sub.used_units['call'].outbox_units == Decimal('0')
    assert out_sub.used_units['call'].inbox_units == Decimal('0')
    assert out_sub.balance == Decimal()

    assert in_sub.used_units['call'].outbox_units == Decimal('0')
    assert in_sub.used_units['call'].inbox_units == Decimal('0')
    assert in_sub.balance == Decimal()

    lp.call_command('2343434 2343433 13:16:59 13:17:58')

    assert out_sub.used_units['call'].outbox_units == Decimal('1')
    assert out_sub.used_units['call'].inbox_units == Decimal('0')
    assert out_sub.balance == Decimal('-2')

    assert in_sub.used_units['call'].outbox_units == Decimal('0')
    assert in_sub.used_units['call'].inbox_units == Decimal('1')
    assert in_sub.balance == Decimal('-1')


# sms_command
def test_sms_command_not_four_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.sms_command('7 7 lite')


def test_sms_command_incorrect_number():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.sms_command('one 1 lite lite')

    with pytest.raises(NotRegisteredSubscriberException):
        lp.sms_command('1 1 lite lite')


def test_sms_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    with pytest.raises(NotActivatedSubscriberException):
        lp.sms_command('2343434 2343433 12:12:12 12:14:12')


def test_sms_command_incorrect_time():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.sms_command('2343434 2343433 12-25-34 12-25-35')


def test_sms_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()
    sc = lp.subs

    # Запоминаем абонентов
    out_sub = lp.subs.get('2343434')
    in_sub = lp.subs.get('2343433')

    assert out_sub.used_units['sms'].outbox_units == Decimal('0')
    assert out_sub.used_units['sms'].inbox_units == Decimal('0')
    assert out_sub.balance == Decimal()

    assert in_sub.used_units['sms'].outbox_units == Decimal('0')
    assert in_sub.used_units['sms'].inbox_units == Decimal('0')
    assert in_sub.balance == Decimal()

    lp.sms_command('2343434 2343433 13:16:59 13:17:58')

    assert out_sub.used_units['sms'].outbox_units == Decimal('1')
    assert out_sub.used_units['sms'].inbox_units == Decimal('0')
    assert out_sub.balance == Decimal('-1')

    assert in_sub.used_units['sms'].outbox_units == Decimal('0')
    assert in_sub.used_units['sms'].inbox_units == Decimal('1')
    assert in_sub.balance == Decimal()


# inet_command
def test_inet_command_not_four_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.inet_command('7 7 lite')


def test_inet_command_incorrect_number():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.inet_command('one 1 lite lite')

    with pytest.raises(NotRegisteredSubscriberException):
        lp.inet_command('1 1 lite lite')


def test_inet_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    with pytest.raises(NotActivatedSubscriberException):
        lp.inet_command('2343434 13 11 12:14:12')


def test_inet_command_incorrect_time():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.inet_command('2343434 13 11 12-25-35')


def test_inet_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()

    # Запоминаем абонентов
    sub = lp.subs.get('2343434')

    assert sub.used_units['inet'].outbox_units == Decimal('0')
    assert sub.used_units['inet'].inbox_units == Decimal('0')
    assert sub.balance == Decimal()

    lp.inet_command('2343434 13 11 13:17:58')

    assert sub.used_units['inet'].outbox_units == Decimal('11')
    assert sub.used_units['inet'].inbox_units == Decimal('13')

    # Т.к. в тарифе интернет бесплатный,
    # никакая плата не должна сниматься.
    assert sub.balance == Decimal()


# change_rate_command
def test_change_rate_command_not_four_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.change_rate_command('7 7 lite')

    with pytest.raises(BadDescriptionLineException):
        lp.change_rate_command('7 7 lite 33 dsf')


def test_change_rate_command_not_registered_rate():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredRateException):
        lp.change_rate_command('1 very_hard 1 12:12:12')


def test_change_rate_command_not_decimal_cost():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDecimalNumberException):
        lp.change_rate_command('1 lite_rate one_dollar 12:12:12')


def test_change_rate_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.change_rate_command('2343434 lite_rate 234 12-25-35')


def test_change_rate_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.rates['hard'] = get_hard_rate()

    sub = lp.subs.get('2343434')
    assert isinstance(sub, Sub)

    sub.is_activated = False
    with pytest.raises(NotActivatedSubscriberException):
        lp.change_rate_command('2343434 hard_rate 50 12:14:12')


def test_change_rate_command_equal_rates():
    lp = get_log_parser_with_two_different_subscribers()

    sub = lp.subs.get('2343434')
    sub.after_rate_pay_passed = 20

    rate_name_before = sub.rate.name
    after_rate_pay_passed_before = sub.after_rate_pay_passed
    balance_before = sub.balance

    lp.change_rate_command('2343434 {} 234 12:25:35'.format(rate_name_before))

    assert sub.rate.name == rate_name_before
    assert sub.after_rate_pay_passed == after_rate_pay_passed_before
    assert sub.balance == balance_before


def test_change_rate_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    # Проверяем, что абонента с номером
    # adding_sub_num ещё не существует.
    adding_sub_num = '89101336754'
    assert lp.subs.get(adding_sub_num) is None

    # Устанавливаем дату.
    lp.day_command('8.12.2016')

    lp.change_rate_command('{} lite_rate 234 12:12:12'.format(adding_sub_num))

    added_sub = lp.subs.get(adding_sub_num)

    assert added_sub is not None

    assert added_sub.rate == get_lite_rate()
    assert added_sub.after_rate_pay_passed == 0

    assert len(added_sub.used_units) == 3
    assert added_sub.used_units['call'] == UsedUnits()
    assert added_sub.used_units['sms'] == UsedUnits()
    assert added_sub.used_units['inet'] == UsedUnits()

    assert added_sub._add_packets == {}
    assert added_sub._day_fee_for_add_packets == Decimal()

    assert added_sub.is_activated is True
    assert added_sub.balance == Decimal()


def test_change_rate_command_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()
    lp.rates['hard_rate'] = get_hard_rate()

    # Проверяем, что абонента с номером
    # adding_sub_num ещё не существует.
    sub = lp.subs.get('2343434')
    balance_before = sub.balance

    # Устанавливаем дату.
    lp.day_command('3.07.2016')

    # Меняем тариф
    lp.change_rate_command('{} hard_rate 234 12:12:12'.format(sub.number))

    assert sub.rate.name == 'hard_rate'
    assert sub.balance == balance_before + Decimal('-234')
    assert sub.after_rate_pay_passed == 0

    assert len(sub.used_units) == 3
    assert sub.used_units['call'] == UsedUnits()
    assert sub.used_units['sms'] == UsedUnits()
    assert sub.used_units['inet'] == UsedUnits()


# add_money_command
def test_add_money_command_not_three_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.add_money_command('7 7 lite 123')

    with pytest.raises(BadDescriptionLineException):
        lp.add_money_command('7 7')


def test_add_money_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.add_money_command('1 1 12:12:12')


def test_add_money_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    lp.add_money_command('2343434 50 12:14:12')


def test_add_money_command_not_decimal_sum():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDecimalNumberException):
        lp.add_money_command('2343434 one_dollar 12:12:12')


def test_add_money_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.add_money_command('2343434 234 12-25-35')


def test_add_money_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()

    sub = lp.subs.get('2343434')
    assert sub.balance == Decimal()

    # Добавляем деньги
    lp.add_money_command('{} 201 12:12:12'.format(sub.number))

    # Баланс должен стать равен 201.
    assert sub.balance == Decimal('201')


# activate_subscriber_command
def test_activate_subscriber_command_not_two_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.activate_subscriber_command('7 7 lite')

    with pytest.raises(BadDescriptionLineException):
        lp.activate_subscriber_command('7')


def test_activate_subscriber_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.activate_subscriber_command('1 12:12:12')


def test_activate_subscriber_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    lp.activate_subscriber_command('2343434 12:14:12')


def test_activate_subscriber_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.activate_subscriber_command('2343434 12-25-35')


def test_activate_subscriber_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    lp.activate_subscriber_command('{} 12:12:12'.format(sub.number))

    assert sub.is_activated is True


# deactivate_subscriber_command
def test_deactivate_subscriber_command_not_two_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.deactivate_subscriber_command('7 7 lite')

    with pytest.raises(BadDescriptionLineException):
        lp.deactivate_subscriber_command('7')


def test_deactivate_subscriber_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.deactivate_subscriber_command('1 12:12:12')


def test_deactivate_subscriber_command_not_activated_subscriber():
    lp = get_log_parser_with_two_different_subscribers()
    lp.subs.get('2343434').is_activated = False

    lp.deactivate_subscriber_command('2343434 12:14:12')


def test_deactivate_subscriber_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDatetimeLineException):
        lp.deactivate_subscriber_command('2343434 12-25-35')


def test_deactivate_subscriber_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    lp.deactivate_subscriber_command('{} 12:12:12'.format(sub.number))

    assert not sub.is_activated


# connect_additional_service_command
def test_connect_additional_service_command_not_three_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.connect_additional_service_command('7 7 lite 2')

    with pytest.raises(BadDescriptionLineException):
        lp.connect_additional_service_command('7 1')


def test_connect_additional_service_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.connect_additional_service_command('1 lite 12:12:12')


def test_connect_additional_service_command_not_registered_add_serv():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredAdditionalPacketException):
        lp.connect_additional_service_command('2343434 lite 12:12:12')


def test_connect_additional_service_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    add_packet = get_lite_add_packet()
    lp.add_packets[add_packet.name] = add_packet

    with pytest.raises(BadDatetimeLineException):
        lp.connect_additional_service_command('2343434 useless 12-25-35')


def test_connect_additional_service_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()

    sub = lp.subs.get('2343434')
    balance_before = sub.balance

    # Описываем доп. услугу
    add_packet = get_lite_add_packet()
    lp.add_packets[add_packet.name] = add_packet

    # Проверяем, что эта доп. услуга ещё не подключена
    assert add_packet not in sub._add_packets.values()

    # Подключаем доп. услугу
    lp.connect_additional_service_command('2343434 useless 12:25:35')

    # Проверяем изменения абонента
    assert balance_before - sub.balance == Decimal('2')
    assert sub._day_fee_for_add_packets == Decimal('2')

    assert add_packet in sub._add_packets.values()


# disconnect_additional_service_command
def test_disconnect_additional_service_command_not_three_arguments():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDescriptionLineException):
        lp.disconnect_additional_service_command('7 7 lite 2')

    with pytest.raises(BadDescriptionLineException):
        lp.disconnect_additional_service_command('7 1')


def test_disconnect_additional_service_command_not_registered_sub():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredSubscriberException):
        lp.disconnect_additional_service_command('1 lite 12:12:12')


def test_disconnect_additional_service_command_not_registered_add_serv():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredAdditionalPacketException):
        lp.disconnect_additional_service_command('2343434 lite 12:12:12')


def test_disconnect_additional_service_command_incorrect_time_format():
    lp = get_log_parser_with_two_different_subscribers()

    add_packet = get_lite_add_packet()
    lp.add_packets[add_packet.name] = add_packet

    with pytest.raises(BadDatetimeLineException):
        lp.disconnect_additional_service_command('2343434 useless 12-25-35')


def test_disconnect_additional_service_command_good_work():
    lp = get_log_parser_with_two_different_subscribers()

    # Описываем доп. услугу
    add_packet = get_lite_add_packet()
    lp.add_packets[add_packet.name] = add_packet

    # Запоминаем абонента и подключаем доп. услугу
    sub = lp.subs.get('2343434')
    lp.connect_additional_service_command('2343434 useless 12:25:35')

    day_fee_before = sub._day_fee_for_add_packets

    # Проверяем, что эта доп. услуга подключена
    assert sub._add_packets.get(add_packet.name) == add_packet
    assert sub._day_fee_for_add_packets == add_packet.subscription_fee

    # Отключаем у абонента доп. услугу
    lp.disconnect_additional_service_command('2343434 useless 12:25:35')

    # Проверяем изменения абонента
    assert sub._add_packets.get(add_packet.name) is None

    sum = sub._day_fee_for_add_packets + add_packet.subscription_fee
    assert sum == day_fee_before


# date_line_to_datetime_obj = date_line_to_dt_obj
def test_date_line_to_dt_obj_bad_attribute():
    with pytest.raises(BadDatetimeLineException):
        LogParser.date_line_to_datetime_obj(1)


def test_date_line_to_dt_obj_bad_datetime_line():
    with pytest.raises(BadDatetimeLineException):
        LogParser.date_line_to_datetime_obj('21-08-1997')


def test_date_line_to_dt_obj_good_work():
    dt_obj = LogParser.date_line_to_datetime_obj('21.08.1997')

    assert isinstance(dt_obj, time.struct_time)

    assert dt_obj[0] == 1997
    assert dt_obj[1] == 8
    assert dt_obj[2] == 21

    assert dt_obj[8] == 0  # Смещение часового пояса.


# time_line_to_datetime_obj = time_line_to_dt_obj
def test_time_line_to_dt_obj_bad_attribute():
    with pytest.raises(BadDatetimeLineException):
        LogParser.time_line_to_datetime_obj(1)


def test_time_line_to_dt_obj_bad_datetime_line():
    with pytest.raises(BadDatetimeLineException):
        LogParser.date_line_to_datetime_obj('20 25 31')


def test_time_line_to_dt_obj_good_work():
    dt_obj = LogParser.time_line_to_datetime_obj('20:25:31')

    assert isinstance(dt_obj, time.struct_time)

    assert dt_obj[3] == 20
    assert dt_obj[4] == 25
    assert dt_obj[5] == 31

    assert dt_obj[8] == 0  # Смещение часового пояса.


# call_length_in_minutes = call_length
def test_call_length_start_more_end():
    start_dt_obj = LogParser.time_line_to_datetime_obj('13:17:00')
    end_dt_obj = LogParser.time_line_to_datetime_obj('13:16:59')

    with pytest.raises(AttributeError):
        LogParser.call_length_in_minutes(start_dt_obj, end_dt_obj)


def test_call_length_good_work():
    start_dt_obj = LogParser.time_line_to_datetime_obj('13:16:59')
    end_dt_obj = LogParser.time_line_to_datetime_obj('13:17:00')

    call_length = LogParser.call_length_in_minutes(start_dt_obj, end_dt_obj)
    assert call_length == 1


# check_line_on_count_components
def test_check_line_on_count_components_good_line():
    lp = get_log_parser_with_two_different_subscribers()

    example_line = ('исходящий_номер входящий_номер '
                    'время_соединения время_разъединения')

    split_line = '2343434 2343433 13:16:59 13:17:58'.split()
    components_count = 4

    lp.check_line_on_count_components(example_line, split_line,
                                      components_count)


def test_check_line_on_count_components_bad_line():
    lp = get_log_parser_with_two_different_subscribers()

    example_line = ('исходящий_номер входящий_номер '
                    'время_соединения время_разъединения')

    split_line = '2343434 50 13:16:59'.split()
    components_count = 4

    with pytest.raises(BadDescriptionLineException):
        lp.check_line_on_count_components(example_line, split_line,
                                          components_count)


# check_subscriber_on_activated
def test_check_subscriber_by_activated_activated_sub():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    lp.check_subscriber_on_activated(sub)


def test_check_subscriber_by_activated_deactivated_sub():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')
    sub.is_activated = False

    with pytest.raises(NotActivatedSubscriberException):
        lp.check_subscriber_on_activated(sub)


# check_subscriber_on_registered
def test_check_subscriber_on_registered_good():
    lp = get_log_parser_with_two_different_subscribers()
    lp.check_subscriber_on_registered('2343434')


def test_check_subscriber_on_registered_bad():
    lp = get_log_parser_with_two_different_subscribers()
    with pytest.raises(NotRegisteredSubscriberException):
        lp.check_subscriber_on_registered('number')


# get_decimal_number
def test_get_decimal_number_correct_arg():
    lp = get_log_parser_with_two_different_subscribers()
    result = lp.get_decimal_number('3.45', 'my_arg')
    assert result == Decimal('3.45')


def test_get_decimal_number_incorrect_arg():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(BadDecimalNumberException):
        lp.get_decimal_number('3.45f', 'my_arg')


# check_rate_on_registered
def test_check_rate_on_registered_good():
    lp = get_log_parser_with_two_different_subscribers()
    lp.check_rate_on_registered('lite_rate')


def test_check_rate_on_registered_bad():
    lp = get_log_parser_with_two_different_subscribers()
    with pytest.raises(NotRegisteredRateException):
        lp.check_rate_on_registered('strange')


# check_add_packet_on_registered
def test_check_add_packet_on_registered_good():
    lp = get_log_parser_with_two_different_subscribers()

    add_packet = get_lite_add_packet()
    lp.add_packets[add_packet.name] = add_packet

    lp.check_add_packet_on_registered('useless')


def test_check_add_packet_on_registered_bad():
    lp = get_log_parser_with_two_different_subscribers()

    with pytest.raises(NotRegisteredAdditionalPacketException):
        lp.check_add_packet_on_registered('lite_add_serv')


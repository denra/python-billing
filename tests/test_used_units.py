from used_units import UsedUnits
from decimal import Decimal


def test_init():
    used_units = UsedUnits()

    assert used_units.inbox_units == Decimal()
    assert used_units.outbox_units == Decimal()

    used_units = UsedUnits(Decimal(1), Decimal(2))

    assert used_units.inbox_units == Decimal(1)
    assert used_units.outbox_units == Decimal(2)


def test_eq():
    used_units1 = UsedUnits(Decimal(1), Decimal(2))
    used_units2 = UsedUnits(Decimal(1), Decimal(2))

    assert used_units1 == used_units2


def test_hash():
    used_units = UsedUnits(Decimal(1), Decimal(2))

    inbox_hash = hash(used_units.inbox_units)
    outbox_hash = hash(used_units.outbox_units)

    assert hash(used_units) == inbox_hash + outbox_hash

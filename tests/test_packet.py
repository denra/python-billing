from tests.support import get_packet

import pytest
from decimal import Decimal


def test_init():
    assert get_packet()


def test_properties_read():
    p = get_packet()

    assert p.name == 'packet_name'
    assert p.type == 'packet_type'

    assert p.free_outbox_units == Decimal(10)
    assert p.free_inbox_units == Decimal(5)

    assert p.extra_outbox_cost == Decimal(2)
    assert p.extra_inbox_cost == Decimal(1)

    assert p.subscription_fee == Decimal(37)


def test_properties_write():
    p = get_packet()

    with pytest.raises(AttributeError):
        p.name = 'packet_name'
    with pytest.raises(AttributeError):
        p.type = 'packet_type'

    with pytest.raises(AttributeError):
        p.free_outbox_units = Decimal(10)
    with pytest.raises(AttributeError):
        p.free_inbox_units = Decimal(5)

    with pytest.raises(AttributeError):
        p.extra_outbox_cost = Decimal(2)
    with pytest.raises(AttributeError):
        p.extra_inbox_cost = Decimal(1)

    with pytest.raises(AttributeError):
        p.subscription_fee = Decimal(37)


def test_eq():
    packet1 = get_packet()
    packet2 = get_packet()

    assert packet1 == packet2


def test_hash():
    packet1 = get_packet()
    packet2 = get_packet()

    assert hash(packet1) == hash(packet2)

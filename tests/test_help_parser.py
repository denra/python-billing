import os
import pytest

from help_parser import HelpParser
from tests.support import get_lite_rate, get_lite_add_packet, get_normal_rate
from exceptions import BadDescriptionLineException

TEST_PATH = os.path.split(__file__)[0]


def get_parser_with_rate_packets():
    h_parser = HelpParser()

    # Добавляем пакеты
    packets = get_lite_rate().packets
    for packet in packets:
        h_parser.data['rate_packets'][packet.name] = packet

    return h_parser


def get_expected_parser_for_file():
    text = ['rate_packets',
            'norm_calls call 0 0 12 11 0',
            'norm_sms sms 0 0 11 0 0',
            'norm_inet inet 10 10 0 0 0',
            '    ',
            'rates',
            'norm_rate [norm_calls, norm_sms, norm_inet] 50 30',
            '\t\t  \t',
            'add_packets',
            'useless other 0 0 0 0 2']

    # Т.к. test_handle_line выполняется,
    # будем уверены, что это работает правильно.
    h_parser = HelpParser()
    for line in text:
        h_parser.handle_line(line)

    return h_parser


def test_get_rate_packets():
    h_parser = get_parser_with_rate_packets()

    res_packs = h_parser._get_rate_packets('lite_rate [lite_calls, lite_sms,'
                                           'lite_inet] 40 30')

    # Следует из get_parser_with_rate_packets
    assert len(res_packs) == len(h_parser.data['rate_packets'])

    for pack in res_packs:
        assert pack == h_parser.data['rate_packets'][pack.name]


def test_get_rate_packets_error():
    h_parser = get_parser_with_rate_packets()

    with pytest.raises(BadDescriptionLineException):
        h_parser._get_rate_packets('lite_rate [lite_calls, lite_sms,'
                                   'bad_pack] 40 30')


def test_input_rate():
    h_parser = get_parser_with_rate_packets()

    assert h_parser.data['rates'] == {}

    h_parser._input_rate('lite_rate [lite_calls, lite_sms,'
                         'lite_inet] 12 30')

    lite_rate = get_lite_rate()
    assert h_parser.data['rates'][lite_rate.name] == lite_rate


def test_input_packets():
    h_parser = HelpParser()
    useless_pack = get_lite_add_packet()
    call_pack = get_lite_rate().packets[0]

    assert len(h_parser.data['add_packets']) == 0
    assert len(h_parser.data['rate_packets']) == 0

    # Добавление доп. пакетов
    h_parser._current_command = 'add_packets'
    h_parser._input_packet('useless other 0 0 0 0 2')

    assert len(h_parser.data['add_packets']) == 1
    assert h_parser.data['add_packets'][useless_pack.name] == useless_pack

    # Добавление пакетов тарифа
    h_parser._current_command = 'rate_packets'
    h_parser._input_packet('lite_calls call 0 0 2 1 0')

    assert len(h_parser.data['rate_packets']) == 1
    assert h_parser.data['rate_packets'][call_pack.name] == call_pack


def test_handle_line():
    text = ['rate_packets',
            'norm_calls call 0 0 12 11 0',
            'norm_sms sms 0 0 11 0 0',
            'norm_inet inet 10 10 0 0 0',
            '    ',
            'rates',
            'norm_rate [norm_calls, norm_sms, norm_inet] 50 30',
            '\t\t  \t\r',
            'add_packets',
            'useless other 0 0 0 0 2']

    h_parser = HelpParser()
    norm_rate = get_normal_rate()
    add_pack = get_lite_add_packet()

    for line in text:
        h_parser.handle_line(line)

    # Пакеты тарифов
    assert len(h_parser.data['rate_packets']) == len(norm_rate.packets)
    for pack in norm_rate.packets:
        assert h_parser.data['rate_packets'][pack.name] == pack

    # Тарифы
    assert h_parser.data['rates'][norm_rate.name] == norm_rate

    # Доп. пакеты
    assert len(h_parser.data['add_packets']) == 1
    assert h_parser.data['add_packets'][add_pack.name] == add_pack


def test_handle_description_file():
    testing_h_parser = HelpParser()
    expected_h_parser = get_expected_parser_for_file()

    full_path = os.path.join(TEST_PATH, 'description_file')
    testing_h_parser.handle_description_file(full_path)

    assert testing_h_parser.data == expected_h_parser.data



from tests.support import (get_sub, get_sub2, get_rate1, get_rate2,
                           get_add_packet,
                           get_log_parser_with_two_different_subscribers)
from used_units import UsedUnits
from sub import Sub

import pytest
from decimal import Decimal


def test_init():
    assert get_sub()


def test_open_fields():
    sub = get_sub()

    assert sub.number == '89173405623'

    assert sub.rate == get_rate1()
    assert sub.after_rate_pay_passed == 0

    assert sub.is_activated is True
    assert sub.balance == Decimal()


def test_close_fields():
    sub = get_sub()

    assert isinstance(sub.used_units, dict)
    assert len(sub.used_units) == 1

    used_units = sub.used_units['packet_type']
    assert used_units.inbox_units == Decimal()
    assert used_units.outbox_units == Decimal()

    assert isinstance(sub._add_packets, dict)
    assert len(sub._add_packets) == 0

    assert sub._day_fee_for_add_packets == Decimal()


def test_traffic_command():
    # 1) Кол-во траффика
    # 2) Кол-во списанных денег
    sub = get_sub()

    assert sub.used_units['packet_type'].outbox_units == Decimal()
    assert sub.used_units['packet_type'].inbox_units == Decimal()

    # 1. Исходящие
    sub.traffic_command('packet_type', Decimal('12'), True)
    assert sub.used_units['packet_type'].outbox_units == Decimal('12')

    # Кол-во бесплатных = 10
    # Стоимость пакета сверх бесплатных = 2
    assert sub.balance == Decimal('-4')

    # 2. Входящие
    sub.traffic_command('packet_type', Decimal('10'), False)
    assert sub.used_units['packet_type'].inbox_units == Decimal('10')
    # Кол-во бесплатных = 5
    # Стоимость единицы трафика сверх бесплатных = 1
    # Помним, что на счету уже -4
    assert sub.balance == Decimal('-9')


def test_call_command():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    # 1. Исходящие
    sub.call_command(Decimal('10'), True)
    assert sub.used_units['call'].outbox_units == Decimal('10')
    assert sub.balance == Decimal('-20')

    # 2. Входящие
    sub.call_command(Decimal('5'), False)
    assert sub.used_units['call'].inbox_units == Decimal('5')
    assert sub.balance == Decimal('-25')


def test_sms_command():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    # 1. Исходящие
    sub.sms_command(Decimal('10'), True)
    assert sub.used_units['sms'].outbox_units == Decimal('10')
    assert sub.balance == Decimal('-10')

    # 2. Входящие
    sub.sms_command(Decimal('5'), False)
    assert sub.used_units['sms'].inbox_units == Decimal('5')
    assert sub.balance == Decimal('-10')


def test_inet_command():
    lp = get_log_parser_with_two_different_subscribers()
    sub = lp.subs.get('2343434')

    # 1. Исходящие
    sub.inet_command(Decimal('10'), True)
    assert sub.used_units['inet'].outbox_units == Decimal('10')
    assert sub.balance == Decimal('0')

    # 2. Входящие
    sub.inet_command(Decimal('5'), False)
    assert sub.used_units['inet'].inbox_units == Decimal('5')
    assert sub.balance == Decimal('0')


def test_change_rate():
    sub = get_sub2()
    sub.after_rate_pay_passed = 20

    # Условия как из test_get_money_for_extra_units
    assert len(sub.used_units) == 3

    assert 'packet_type' in sub.used_units
    assert 'unbound' in sub.used_units
    assert 'other_pack' in sub.used_units

    assert sub.balance == Decimal()
    assert sub.rate == get_rate2()

    sub.change_rate(get_rate1(), Decimal('90'))

    assert sub.balance == Decimal('-90')
    assert sub.rate == get_rate1()
    assert sub.after_rate_pay_passed == 0

    assert len(sub.used_units) == 1
    assert 'packet_type' in sub.used_units


def test_add_money_command_right():
    sub = get_sub()

    assert sub.balance == Decimal()

    sub.add_money_command(Decimal(10))
    assert sub.balance == Decimal(10)

    sub.add_money_command(Decimal())
    assert sub.balance == Decimal(10)


def test_add_money_command_error():
    sub = get_sub()
    with pytest.raises(AttributeError):
        sub.add_money_command(Decimal(-10))


def test_activate():
    sub = get_sub()

    sub.is_activated = False
    assert sub.is_activated is False

    sub.activate()
    assert sub.is_activated is True


def test_deactivate():
    sub = get_sub()

    assert sub.is_activated is True

    sub.deactivate()
    assert sub.is_activated is False


def test_connect_add_packet():
    sub = get_sub()

    assert len(sub._add_packets) == 0
    assert sub._day_fee_for_add_packets == Decimal()
    assert sub.balance == Decimal()

    sub.connect_add_packet(get_add_packet())

    assert len(sub._add_packets) == 1
    assert sub._add_packets['add_packet'] == get_add_packet()

    assert sub._day_fee_for_add_packets == Decimal('3.4')
    assert sub.balance == Decimal('-3.4')


def test_disconnect_add_packet():
    sub = get_sub()
    sub.connect_add_packet(get_add_packet())

    assert len(sub._add_packets) == 1
    assert sub._add_packets['add_packet'] == get_add_packet()

    assert sub._day_fee_for_add_packets == Decimal('3.4')

    sub.disconnect_add_packet('add_packet')

    assert len(sub._add_packets) == 0
    assert sub._day_fee_for_add_packets == Decimal()


def test_disconnect_add_packet_not_connected_add_packet():
    sub = get_sub()

    assert len(sub._add_packets) == 0

    assert sub._day_fee_for_add_packets == Decimal()

    sub.disconnect_add_packet('add_packet')

    assert len(sub._add_packets) == 0
    assert sub._day_fee_for_add_packets == Decimal()


def test_pay_add_packets():
    sub = get_sub()
    sub._day_fee_for_add_packets = Decimal('10')

    assert sub.balance == Decimal()
    sub.pay_add_packets()
    assert sub.balance == Decimal('-10')


def test_pay_rate():
    """
    Абонентская плата за тариф = 100
    """
    sub = get_sub2()

    # Проверка pay_rate
    assert sub.balance == Decimal()
    sub.pay_rate()
    assert sub.balance == Decimal('-100')


def test__get_money_for_extra_units():
    """
    3 пакета в тарифе:
    1) Полностью бесплатно
    2) Использовано больше, чем кол-во бесплатных.
    3) Использовано меньше, чем кол-во бесплатных.

    Пакет 1:
    Кол-во бесплатных исходящих единиц = inf
    Кол-во бесплатных входящих единиц = inf
    Кол-во стоимость сверх бесплат. исходящих = 2
    Кол-во стоимость сверх бесплат. исходящих = 1

    Пакет 2:
    Кол-во бесплатных исходящих единиц = 10
    Кол-во бесплатных входящих единиц = 5
    Кол-во стоимость сверх бесплат. исходящих = 2
    Кол-во стоимость сверх бесплат. исходящих = 1

    Пакет 3:
    Кол-во бесплатных исходящих единиц = 10
    Кол-во бесплатных входящих единиц = 5
    Кол-во стоимость сверх бесплат. исходящих = 2
    Кол-во стоимость сверх бесплат. исходящих = 1
    """
    sub = get_sub2()

    # Вспомогательные переменные
    inf = Decimal('inf')
    very_many = Decimal('1000000000000000000')

    # 1) Здесь должен быть 0
    sub.used_units['unbound'].outbox_units = inf
    sub.used_units['unbound'].inbox_units = very_many

    assert sub._Sub__get_money_for_extra_units() == Decimal()

    # 2) (40 - 10) * 2 + (30 - 5) = 30 * 2 + 25 = 85
    sub.used_units['packet_type'].outbox_units = Decimal('40')
    sub.used_units['packet_type'].inbox_units = Decimal('30')

    assert sub._Sub__get_money_for_extra_units() == Decimal('85')

    # 3) (4 - 10) * 2 + (10 - 5) = 0 + 5 = 5
    sub.used_units['other_pack'].outbox_units = Decimal('4')
    sub.used_units['other_pack'].inbox_units = Decimal('10')

    assert sub._Sub__get_money_for_extra_units() == Decimal('90')


def test_get_extra_units():
    """
    3 случая:
    1) inf - любое кол-во бесплатных
    2) > 0 - привышен порог бесплатных единиц
    3) <= 0 - порог не привышен.
    """
    inf = Decimal('inf')
    seventy = Decimal('70')
    fifty = Decimal('50')
    twenty = Decimal('20')
    null = Decimal()

    # 1)
    assert Sub._Sub__get_extra_units(inf, inf) == null
    assert Sub._Sub__get_extra_units(inf, fifty) == null
    assert Sub._Sub__get_extra_units(inf, null) == null

    # 2)
    assert Sub._Sub__get_extra_units(fifty, seventy) == twenty

    # 3)
    assert Sub._Sub__get_extra_units(fifty, fifty) == null
    assert Sub._Sub__get_extra_units(fifty, twenty) == null


def test_get_extra_traffic_bad_attribute():
    sub = get_sub()

    with pytest.raises(AttributeError):
        sub.get_extra_traffic(Decimal(), Decimal('5'), Decimal())


def test_get_extra_traffic_first_stage():
    sub = get_sub()

    args = [Decimal('10'), Decimal('3'), Decimal('7')]
    expected = Decimal()

    assert sub.get_extra_traffic(*args) == expected


def test_get_extra_traffic_second_stage():
    sub = get_sub()

    args = [Decimal('10'), Decimal('9'), Decimal('14')]
    expected = Decimal('4')

    assert sub.get_extra_traffic(*args) == expected


def test_get_extra_traffic_third_stage():
    sub = get_sub()

    args = [Decimal('10'), Decimal('13'), Decimal('17')]
    expected = Decimal('4')

    assert sub.get_extra_traffic(*args) == expected

from packet import Packet
from rate import Rate
from sub import Sub
from log_parser import LogParser

from decimal import Decimal


def get_packet():
    args = ['packet_name', 'packet_type',
            Decimal(10), Decimal(5),
            Decimal(2), Decimal(1),
            Decimal(37)]
    return Packet(*args)


def get_unbound_packet():
    args = ['unbound_pack', 'unbound',
            Decimal('inf'), Decimal('inf'),
            Decimal(2), Decimal(1),
            Decimal(37)]
    return Packet(*args)


def get_other_packet():
    args = ['other_pack', 'other_pack',
            Decimal(10), Decimal(5),
            Decimal(2), Decimal(1),
            Decimal(37)]
    return Packet(*args)


def get_add_packet():
    args = ['add_packet', 'other',
            Decimal(), Decimal(),
            Decimal(), Decimal(),
            Decimal('3.4')]
    return Packet(*args)


def get_useless_add_packet():
    args = ['useless', 'other',
            Decimal(), Decimal(),
            Decimal(), Decimal(),
            Decimal()]
    return Packet(*args)


def get_lite_add_packet():
    args = ['useless', 'other',
            Decimal(), Decimal(),
            Decimal(), Decimal(),
            Decimal('2')]
    return Packet(*args)


def get_rate1():
    args = ['rate_name', [get_packet()], Decimal('100')]
    return Rate(*args)


def get_rate2():
    args = ['rate_name_2',
            [get_unbound_packet(), get_packet(), get_other_packet()],
            Decimal('100')]
    return Rate(*args)


def get_lite_rate():
    null = Decimal()
    inf = Decimal('inf')

    calls_pack = Packet('lite_calls', 'call', null, null, Decimal('2'),
                        Decimal('1'), null)
    sms_pack = Packet('lite_sms', 'sms', null, inf, Decimal('1'), null, null)
    inet_pack = Packet('lite_inet', 'inet', inf, inf, null, null, null)

    args = ['lite_rate', [calls_pack, sms_pack, inet_pack], Decimal('12')]
    return Rate(*args)


def get_hard_rate():
    null = Decimal()
    inf = Decimal('inf')

    calls_pack = Packet('hard_calls', 'call', null, null, Decimal('12'),
                        Decimal('11'), null)
    sms_pack = Packet('hard_sms', 'sms', null, inf, Decimal('11'), null, null)
    inet_pack = Packet('hard_inet', 'inet', Decimal('10'), Decimal('10'),
                       null, null, null)

    args = ['hard_rate', [calls_pack, sms_pack, inet_pack], Decimal('112')]
    return Rate(*args)


def get_normal_rate():
    null = Decimal()

    calls_pack = Packet('norm_calls', 'call', null, null, Decimal('12'),
                        Decimal('11'), null)
    sms_pack = Packet('norm_sms', 'sms', null, null, Decimal('11'), null, null)
    inet_pack = Packet('norm_inet', 'inet', Decimal('10'), Decimal('10'),
                       null, null, null)

    args = ['norm_rate', [calls_pack, sms_pack, inet_pack], Decimal('50')]
    return Rate(*args)


def get_sub():
    args = ['89173405623', get_rate1()]
    return Sub(*args)


def get_sub2():
    args = ['89173405621', get_rate2()]
    sub = Sub(*args)

    return sub


def get_log_parser_with_two_different_subscribers():
    lite_rate = get_lite_rate()
    sub1 = Sub('2343434', lite_rate)
    sub2 = Sub('2343433', lite_rate)

    rates = {'lite_rate': lite_rate, 'hard_rate': get_hard_rate()}
    add_packets = {}
    subs = {'2343434': sub1, '2343433': sub2}

    return LogParser(rates, add_packets, subs)


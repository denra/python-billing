from tests.support import get_rate1

from decimal import Decimal
import pytest


def test_init():
    assert get_rate1()


def test_properties_read():
    rate = get_rate1()

    assert rate.name == 'rate_name'
    assert rate.subscription_fee == Decimal(100)

    assert isinstance(rate.packets, list)
    assert len(rate.packets) == 1

    # Внимание! В настоящее время пакеты тарифа можно изменять.
    # Это сделано для того, чтобы тратить меньше времени и
    # памяти во время обработки лога.
    rate.packets.append(1)
    assert len(rate.packets) == 2


def test_properties_write():
    rate = get_rate1()

    with pytest.raises(AttributeError):
        rate.name = 'packet_name'
    with pytest.raises(AttributeError):
        rate.subscription_fee = Decimal(37)
    with pytest.raises(AttributeError):
        rate.packets = []


def test_eq():
    rate1 = get_rate1()
    rate2 = get_rate1()

    assert rate1 == rate2


def test_hash():
    rate1 = get_rate1()
    rate2 = get_rate1()

    assert hash(rate1) == hash(rate2)


def test_get_packet_not_found():
    rate = get_rate1()
    assert rate.get_packet('unexistent') is None


def test_get_packet():
    rate = get_rate1()
    pack = rate.packets[0]

    assert rate.get_packet(pack.type) == pack

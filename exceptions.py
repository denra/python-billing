__all__ = ['BadDatetimeLineException', 'BadDescriptionLineException',
           'BadDecimalNumberException', 'NotRegisteredSubscriberException',
           'NotRegisteredRateException', 'NotActivatedSubscriberException',
           'NotRegisteredAdditionalPacketException', 'MyException',
           'IntervalEndHasAttainedException']


class MyException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value


class BadDatetimeLineException(MyException):
    pass


class BadDescriptionLineException(MyException):
    pass


class BadDecimalNumberException(MyException):
    pass


class NotRegisteredSubscriberException(MyException):
    pass


class NotRegisteredRateException(MyException):
    pass


class NotRegisteredAdditionalPacketException(MyException):
    pass


class NotActivatedSubscriberException(MyException):
    pass


class IntervalEndHasAttainedException(Exception):
    pass

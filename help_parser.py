import re
import sys
import logging
from typing import List
from decimal import Decimal

from exceptions import BadDescriptionLineException, MyException
from file_opener import safe_open
from packet import Packet
from rate import Rate


class HelpParser:
    def __init__(self):
        self.data = {
            'rate_packets': {},
            'add_packets': {},
            'rates': {}
        }

        self._current_command = None

        self._name_to_class = {
            'rate_packets': Packet,
            'add_packets': Packet,
            'rates': Rate
        }

    def handle_description_file(self, full_path_to_file: str) -> None:
        """Обрабатывает файл с описание доп. пакетов и тарифов.

        :param full_path_to_file: Полный путь к файлу.
        """
        with safe_open(full_path_to_file) as file:
            i = 1
            for line in file:
                try:
                    # Надо передавать строку
                    # без пробельных символов слева и справа.
                    self.handle_line(line.strip())
                except MyException as e:
                    logging.warning('Номер строки: {}\n\n{}'.format(i, e))
                i += 1

    def handle_line(self, line) -> None:
        # Если текущая команда неопределена,
        # не понятно, что делать =>
        # => экстренно завершаем программу.
        if self._current_command is None:
            if line not in self._name_to_class:
                sys.exit(-42)

        # Если строка меняет текущую команду,
        # дальше обрабатывать эту строку не идем.
        if line in self._name_to_class:
            self._current_command = line
            return


        if line.strip() == '':
            return

        # Если вводим пакет
        if self._current_command in ['rate_packets', 'add_packets']:
            self._input_packet(line)
        # Если вводим тариф
        elif self._current_command in ['rates']:
            self._input_rate(line)

    def _input_packet(self, line: str) -> None:
        split_line = line.split()

        # Выделяем компоненты
        pack_name = split_line[0]
        pack_type = split_line[1]
        free_outbox_units = Decimal(split_line[2])
        free_inbox_units = Decimal(split_line[3])
        extra_outbox_cost = Decimal(split_line[4])
        extra_inbox_cost = Decimal(split_line[5])
        subscription_fee = Decimal(split_line[6])

        # Создаем новый пакет
        new_pack = Packet(pack_name, pack_type, free_outbox_units,
                          free_inbox_units, extra_outbox_cost,
                          extra_inbox_cost, subscription_fee)

        # Добавляем к нужной группе пакетов
        self.data[self._current_command][new_pack.name] = new_pack

    def _input_rate(self, line: str) -> None:
        packets = self._get_rate_packets(line)

        # Удаляем подстроку с пакетами
        # для облегчения дальнейшей работы со строкой

        line = re.sub('\[.*?\]', 'packets', line)

        # Выделяем остальные компоненты
        split_line = line.split()

        name = split_line[0]
        subscription_fee = Decimal(split_line[2])
        pay_period = Decimal(split_line[3])

        # Создаем новый тариф
        new_rate = Rate(name, packets, subscription_fee, pay_period)

        # Добавляем к нужной группе пакетов
        self.data['rates'][new_rate.name] = new_rate

    def _get_rate_packets(self, line: str) -> List[Packet]:
        # Выделяем пакеты
        # -- Ищем подстроку с пакетами
        str_packets = re.search('\[.*?\]', line).group()
        
        # -- Убираем квадратные скобки
        str_packets = str_packets[1:len(str_packets) - 1]

        # -- Получаем имена пакетов
        packet_names = str_packets.split(',')
        packet_names = [x.strip() for x in packet_names]

        # -- Ищем соответствующие пакеты
        rate_packets = []
        input_packets = self.data.get('rate_packets')
        for packet_name in packet_names:
            pack = input_packets.get(packet_name)
            
            if not pack:
                raise BadDescriptionLineException(
                    'Указан не существующий пакет')

            rate_packets.append(pack)

        return rate_packets

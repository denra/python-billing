import os
import sys
import logging


def safe_open(full_path_to_file: str) -> open:
    """Безопасно открывает файл.
    Если невозможно открыть файл, программа переводится
    в критический режим.

    :param full_path_to_file: Полный путь к файлу.
    """
    if not os.path.isfile(full_path_to_file):
        logging.critical(
            'safe_open:\n'
            '\t Путь full_path_to_file=\'{}\'\n'
            '\t должен указывать на существующий файл'.format(
                full_path_to_file)
        )
        sys.exit(-42)

    # Как узнать, что у открываемого файла кодировка не utf-8?
    try:
        with open(full_path_to_file, 'r') as file:
            file.read(1)
    except UnicodeDecodeError:
        logging.critical('safe_open: Файл должен быть в кодировке utf-8.')
        sys.exit(-42)

    return open(full_path_to_file, 'r')

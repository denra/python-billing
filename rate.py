from packet import Packet

from typing import List
from decimal import Decimal


class Rate:
    """Этот класс должен использоваться как неизменяемый."""
    def __init__(self, name: str, packets: List[Packet], sub_fee: Decimal,
                 pay_period: int=30) -> None:
        self._name = name

        self._packets = packets
        self._subscription_fee = sub_fee

        self._pay_period = pay_period

    @property
    def name(self) -> str:
        return self._name

    @property
    def packets(self) -> List[Packet]:
        """Внимание! В настоящее время пакеты тарифа можно изменять.
        Пока что принимаем на веру, что этот список
        будут только читать.
        Это сделано для того, чтобы тратить меньше времени и
        памяти во время обработки лога."""
        return self._packets

    @property
    def subscription_fee(self) -> Decimal:
        return self._subscription_fee

    @property
    def pay_period(self):
        return self._pay_period

    def get_packet(self, pack_type: str) -> Packet:
        for pack in self.packets:
            if pack.type == pack_type:
                return pack

    def __eq__(self, other):
        name_eq = self.name == other.name
        pay_period_eq = self.pay_period == other.pay_period
        packets_eq = self.packets == other.packets
        subscription_fee_eq = (self.subscription_fee ==
                               other.subscription_fee)

        return name_eq and packets_eq and pay_period_eq and subscription_fee_eq

    def __hash__(self):
        rate_hash = hash(self._name) + hash(self._subscription_fee)

        for packet in self._packets:
            rate_hash += hash(packet)

        rate_hash += hash(self._pay_period)

        return rate_hash

from decimal import Decimal


class UsedUnits:
    def __init__(self, inbox_units: Decimal=Decimal(),
                 outbox_units: Decimal=Decimal()) -> None:
        self.inbox_units = inbox_units
        self.outbox_units = outbox_units

    def __eq__(self, other):
        return (self.inbox_units == other.inbox_units and
                self.outbox_units == other.outbox_units)

    def __hash__(self):
        return hash(self.inbox_units) + hash(self.outbox_units)

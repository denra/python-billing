from packet import Packet
from rate import Rate
from sub import Sub


from exceptions import (MyException, BadDescriptionLineException,
                        IntervalEndHasAttainedException,
                        BadDatetimeLineException,
                        NotActivatedSubscriberException,
                        NotRegisteredSubscriberException,
                        NotRegisteredRateException,
                        NotRegisteredAdditionalPacketException,
                        BadDecimalNumberException)
from file_opener import safe_open

import logging
import inspect

from typing import Dict, List
from time import struct_time, mktime, strptime, tzname
from decimal import Decimal, InvalidOperation


class LogParser:
    def __init__(self, rates: Dict[str, Rate],
                 add_packets: Dict[str, Packet],
                 subs: Dict[str, Sub]) -> None:
        """Инициализация парсера.
        :param rates: название тарифа -> тариф.
        :param add_packets: название доп. услуги -> доп. услуга.
        """
        # Тарифы, доп. услуги (пакеты), абоненты
        self.rates = rates
        self.add_packets = add_packets
        self.subs = subs

        # Текущий день
        self._current_day = None

        # Промежуток времени, на котором осуществляется поиск, причем
        # если start_day = None,
        # то поиск осуществляется с самого начала, а
        # если end_day = None,
        # то поиск осуществляется до самого конца.
        self.start_day = None
        self.end_day = None

        # Нужно ли детализировать всех абонентов
        self.need_all_detail = False

        # Детализируемые номера
        self.detailing_numbers = None

        # Вспомогательные переменные
        # --------------------------
        self._command_to_func = {
            'day': self.day_command,
            'call': self.call_command,
            'sms': self.sms_command,
            'inet': self.inet_command,
            'change_rate': self.change_rate_command,
            'add_money': self.add_money_command,
            'activate_subscriber': self.activate_subscriber_command,
            'deactivate_subscriber': self.deactivate_subscriber_command,
            'connect_additional_service':
                self.connect_additional_service_command,
            'disconnect_additional_service':
                self.disconnect_additional_service_command
        }
        # --------------------------
        self._command_to_description = {
            'day': 'День {}',
            'call': 'Звонок от {} к {} начался в {}, закончился в {}',
            'sms': 'Смс от {} к {} отправлена в {}, принята в {}',
            'inet': 'Интернет от {}\n'
                    'Кол-во входящего трафика: {}\n'
                    'Кол-во исходящего трафика: {}\n'
                    'Время: {}\n',
            'change_rate': '{} сменил тариф на {}. '
                           'Стоимость операции: {}. Время: {}',
            'add_money': '{} пополнил счет на сумму {} в {}',
            'activate_subscriber': '{} активирован в {}',
            'deactivate_subscriber': '{} заблокирован в {}',
            'connect_additional_service':
                '{} подключил дополнительную услугу {} в {}',
            'disconnect_additional_service':
                '{} отключил дополнительную услугу {} в {}'
        }
        self._command_to_count_args = {
            'day': 1,
            'call': 4,
            'sms': 4,
            'inet': 4,
            'change_rate': 4,
            'add_money': 3,
            'activate_subscriber': 2,
            'deactivate_subscriber': 2,
            'connect_additional_service': 3,
            'disconnect_additional_service': 3
        }
        # --------------------------
    # TODO: протестить handle_log_file, handle_line, get_detailed_line
    # TODO: протестить print_detailed_line

    # Разбирает весь файл
    def handle_log_file(self, full_path_to_file: str) -> None:
        """Обрабатывает лог АТС (построчно).

        :param full_path_to_file: Полный путь к логу.
        """
        with safe_open(full_path_to_file) as file:
            i = 1
            for line in file:
                try:
                    line = line.strip()
                    self.handle_line(line)
                    self.print_detailed_line(self.get_detailed_line(line))
                except MyException as e:
                    logging.warning('Номер строки: {}\n\n{}'.format(i, e))

                # Если достугнут конец интервала,
                # заканчиваем работу.
                except IntervalEndHasAttainedException:
                    return
                i += 1

    # Разбирает строку
    def handle_line(self, line: str) -> None:
        """Разбирает и обрабатывает строку лога.

        :param line: Обрабатываемая строка.
        """
        split_line = line.split()
        command = split_line[0]

        # Если наш обработчик не умеет обрабатывать
        # команду в логе - это исключительный случай.
        if command not in self._command_to_func:
            raise BadDescriptionLineException(
                'Строка должна начинаться с ключевого слова.\n'
                '\n'
                'Исходная строка: {}'.format(line)
            )

        # Если текущая дата не определена,
        # ждем пока появится какая-нибудь дата.
        # Нужно для того, чтобы пропускать даты,
        # которые не входят в интервал (start_day, end_day)
        if self._current_day is None and command != 'day':
            return

        # Обрабатываем команду.
        # К длине команды добавляется 1,
        # т.к. команда и ее параметры разделены пробелом.
        self._command_to_func[command](line[len(command) + 1:])

    # Детализирует строку команды
    def get_detailed_line(self, line: str) -> str:
        """Выводит детализацию, если это необходимо.

        Так как данный метод выполняется в handle_log_file
        после метода handle_line, то можно с увереностью
        считать, что строка line корректна.

        :param line: Детализируемая строка.
        """
        # Если дата не определена,
        # значит мы ждем интервал детализации.
        if not self._current_day:
            return

        split_line = line.split()

        command = split_line[0]

        if not self.need_all_detail:
            if self.detailing_numbers:
                # Если детализируемая строка НЕ относится
                # к self.detailing_number, то возвращаем None.
                if command != 'day':
                    outbox_number = split_line[1]
                    inbox_number = split_line[2]

                    if outbox_number not in self.detailing_numbers:
                        if inbox_number not in self.detailing_numbers:
                            return
            else:
                return

        # Возвращаем детализированную строку
        return self._command_to_description[command].format(*split_line[1:])

    # Печатает детализированную строку
    def print_detailed_line(self, detailed_line: str) -> None:
        """Печатает детализированную строку.

        :param detailed_line: Детализированная строка.
        """
        # Если дата не определена,
        # значит мы ждем интервал детализации
        if not self._current_day:
            return

        # Если нужно детализировать, то делаем это
        if self.need_all_detail or self.detailing_numbers:
            print(detailed_line)

    # Методы обработки различных видов строк журнала.
    def day_command(self, date_line: str) -> None:
        """Обрабатывает команду 'day'.

        Этапы обработки:
            1) Изменяем текущую дату.
            2) С каждого абонента снимаем плату за доп. услуги.
            3) C каждого абонента снимаем ежемесячную абонентскую
            плату, если с момента подключения тарифа
            прошло время == 30-ти календарным дням.

         Формат строки 'команда + параметры':
                day dd.mm.yyyy

        :param date_line: Строка с датой.
        """
        # Пробуем изменить текущую дату.
        try:
            self._current_day = self.date_line_to_datetime_obj(date_line)
        except BadDatetimeLineException as e:
            self._current_day = None
            raise e

        # Если определено начало интервала,
        # на котором необходимо парсить log
        if self.start_day:
            # Если текущий день меньше начального,
            if self._current_day < self.start_day:
                # Пропускаем его
                self._current_day = None
                return

        # Если определен конец интевала,
        # на котором необходимо парсить log
        if self.end_day:
            # Если текущий день больше конца,
            if self._current_day > self.end_day:
                # Прерываем работу log_parser'а
                raise IntervalEndHasAttainedException

        # Считаем плату за предоставляемые услуги.
        for sub in self.subs.values():
            if not sub.is_activated:
                continue

            # Снимаем плату за доп. услуги
            sub.pay_add_packets()

            # Считаем, что кол-во дней с момента последней оплаты
            # тарифа увеличивается в начале каждого дня.
            sub.after_rate_pay_passed += 1

            # Снимаем плату за тариф
            if sub.after_rate_pay_passed == sub.rate.pay_period:
                sub.pay_rate()
                sub.after_rate_pay_passed = 0

    def call_command(self, line: str) -> None:
        """Обрабатывает команду 'call'.

        Этапы обработки:
            1) Проверяем корректность входной строки.
            2) Считаем длительность звонка.
            3) Снимаем плату за исходящее соединение
               с абонента, начавшего звонок.
            4) Снимаем плату за входящее соединение
               с абонента, принявшего звонок.

        Формат строки 'команда + параметры':
                call outbox_num inbox_num start_time end_time

        start_time и end_time имеют формат:
                hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 4-х компонент
        split_line = line.split()
        example_line = 'outbox_num inbox_num start_time end_time'
        self.check_line_on_count_components(example_line, split_line, 4)

        # Первые две компоненты должны описывать
        # номера абонентов, которые уже зарегистрированы в АТС.
        self.check_subscriber_on_registered(split_line[0])
        self.check_subscriber_on_registered(split_line[1])

        # Запоминаем абонентов.
        outbox_sub = self.subs.get(split_line[0])
        inbox_sub = self.subs.get(split_line[1])

        # Проверяем, что абоненты активированы.
        self.check_subscriber_on_activated(outbox_sub)
        self.check_subscriber_on_activated(inbox_sub)

        # # Снимаем плату с учетом длительности звонка.
        #
        # Считаем длительность звонка.
        start_time_obj = LogParser.time_line_to_datetime_obj(split_line[2])
        end_time_obj = LogParser.time_line_to_datetime_obj(split_line[3])

        call_length = LogParser.call_length_in_minutes(start_time_obj,
                                                       end_time_obj)
        call_length = Decimal('{}'.format(call_length))

        # Считаем сумму для абонента с исходящим вызовом.
        outbox_sub.call_command(call_length, is_outbox=True)

        # Считаем сумму для абонента с входящим вызовом.
        inbox_sub.call_command(call_length, is_outbox=False)

    def sms_command(self, line: str) -> None:
        """Обрабатывает команду 'sms'.

        Этапы обработки:
            1) Проверяем корректность входной строки.
            2) Снимаем плату за исходящее смс.

        Формат строки 'команда + параметры':
                sms outbox_num inbox_num send_time receive_time

        send_time и receive_time имеют формат:
                hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 4-х компонент
        split_line = line.split()
        example_line = 'outbox_num inbox_num send_time receive_time'
        self.check_line_on_count_components(example_line, split_line, 4)

        # Первые две компоненты должны описывать
        # номера абонентов, которые уже зарегистрированы в АТС.
        self.check_subscriber_on_registered(split_line[0])
        self.check_subscriber_on_registered(split_line[1])

        # Запоминаем абонентов
        outbox_sub = self.subs.get(split_line[0])
        inbox_sub = self.subs.get(split_line[1])

        # Проверяем, что абоненты активированы.
        self.check_subscriber_on_activated(outbox_sub)
        self.check_subscriber_on_activated(inbox_sub)

        # Проверяем корректность времени
        LogParser.time_line_to_datetime_obj(split_line[2])
        LogParser.time_line_to_datetime_obj(split_line[3])

        # Снимаем плату за отправленную смс.
        outbox_sub.sms_command(Decimal('1'), is_outbox=True)
        inbox_sub.sms_command(Decimal('1'), is_outbox=False)

    def inet_command(self, line: str) -> None:
        """Обрабатывает команду 'inet'.

        Этапы обработки:
            1) Проверяем корректность входной строки.
            2) Снимаем плату за исходящее смс.

        Формат строки 'команда + параметры':
                inet number inbox_traffic outbox_traffic time

        time имеют формат:
                hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 4-х компонент
        split_line = line.split()
        example_line = 'number inbox_traffic outbox_traffic time'
        self.check_line_on_count_components(example_line, split_line, 4)

        # Первая компонента должна описывать
        # номер абонента, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])

        # Запоминаем абонента
        sub = self.subs.get(split_line[0])

        # Проверяем, что абонент активирован.
        self.check_subscriber_on_activated(sub)

        # Проверяем корректность времени
        LogParser.time_line_to_datetime_obj(split_line[3])

        # Проверяем, что inbox_traffic и outbox_traffic
        # могут быть выражены через Decimal
        inbox_traffic = self.get_decimal_number(split_line[1], 'inbox_traffic')
        outbox_traffic = self.get_decimal_number(split_line[2],
                                                 'outbox_traffic')

        # Снимаем плату за отправленную интернет.
        sub.inet_command(inbox_traffic, is_outbox=False)
        sub.inet_command(outbox_traffic, is_outbox=True)

    def change_rate_command(self, line: str) -> None:
        """Обрабатывает команду 'change_rate'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > наименование тарифа должно быть существующим
                 > стоимость смены тарифа должна быть в формате Decimal
                 > время должно быть задано корректным форматом.

            2) Меняем тариф:
               а) Если абонент с принятым номером уже существует,
                  то если тариф абонента отличается
                  от переданного - меняем тариф.

               б) Иначе создаем нового абонента
                  с номером и тарифом, которые мы нам передали.

        Формат строки 'команда + параметры':
                change_rate number rate_name change_cost time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        split_line = line.split()

        # Строка line должна состоять из 4-х компонент
        example_line = 'number rate_name change_cost time'
        self.check_line_on_count_components(example_line, split_line, 4)

        # Вторая компонента должна описывать
        # наименование тарифа, который уже зарегистрирован в АТС.
        self.check_rate_on_registered(split_line[1])
        rate = self.rates.get(split_line[1])

        # Третья компонента должна описывать
        # корректное рациональное число.
        change_cost = self.get_decimal_number(split_line[2], 'change_cost')

        # Четвертая компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[3])

        # # Меняем тариф
        try:
            # Если абонент с таким номером уже зарегистрирован,
            self.check_subscriber_on_registered(split_line[0])

            # то если абонент активен и
            sub = self.subs.get(split_line[0])
            self.check_subscriber_on_activated(sub)

            # его текущий тариф отличается от переданного
            if sub.rate.name != rate.name:
                # меняем его тариф.
                sub.change_rate(rate, change_cost)

        # Иначе
        except NotRegisteredSubscriberException:
            # Создаем нового абонента и
            sub = Sub(split_line[0], rate)

            # добавляем его в контроллер.
            self.subs[sub.number] = sub

    def add_money_command(self, line: str) -> None:
        """Обрабатывает команду 'add_money'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > абонент с переданным номером
                   должен быть зарегистрирован
                 > добавляемая сумма должна быть в формате Decimal
                 > время должно быть задано корректным форматом.

            2) Добавляем сумму на счет абонента

        Формат строки 'команда + параметры':
                add_money number adding_sum time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 3-х компонент
        split_line = line.split()
        self.check_line_on_count_components('number adding_sum time',
                                            split_line, 3)

        # Первая компонента должна описывать
        # наименование тарифа, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])
        sub = self.subs.get(split_line[0])

        # Вторая компонента должна описывать
        # корректное рациональное число.
        adding_sum = self.get_decimal_number(split_line[1], 'adding_sum')

        # Третья компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[2])

        # Добавляем средства абоненту
        sub.balance += adding_sum

    def activate_subscriber_command(self, line: str) -> None:
        """Обрабатывает команду 'activate_subscriber'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > абонент с переданным номером
                   должен быть зарегистрирован
                 > время должно быть задано корректным форматом.

            2) Активируем пользователя.

        Формат строки 'команда + параметры':
                activate_subscriber number time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 2-х компонент
        split_line = line.split()
        self.check_line_on_count_components('number time', split_line, 2)

        # Первая компонента должна описывать
        # номер абонента, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])
        sub = self.subs.get(split_line[0])

        # Вторая компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[1])

        # # Активируем пользователя
        sub.is_activated = True

    def deactivate_subscriber_command(self, line: str) -> None:
        """Обрабатывает команду 'deactivate_subscriber'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > абонент с переданным номером
                   должен быть зарегистрирован
                 > время должно быть задано корректным форматом.

            2) Отключаем пользователя.

        Формат строки 'команда + параметры':
                deactivate_subscriber number time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 2-х компонент
        split_line = line.split()
        self.check_line_on_count_components('number time', split_line, 2)

        # Первая компонента должна описывать
        # номер абонента, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])
        sub = self.subs.get(split_line[0])

        # Вторая компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[1])

        # # Блокируем пользователя
        sub.is_activated = False

    def connect_additional_service_command(self, line: str) -> None:
        """Обрабатывает команду 'connect_additional_service'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > абонент с переданным номером
                   должен быть зарегистрирован;
                 > доп. услуга должна быть зарегистрирована;
                 > время должно быть задано корректным форматом.

            2) Подключаем пользователю доп. услугу.

        Формат строки 'команда + параметры':
                connect_additional_service number add_serv_name time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 3-х компонент
        split_line = line.split()
        self.check_line_on_count_components('number add_serv_name time',
                                            split_line, 3)

        # Первая компонента должна описывать
        # номер абонента, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])
        sub = self.subs.get(split_line[0])

        # Вторая компонента должна описывать
        # наименование доп. услуги, который уже зарегистрирован в АТС.
        self.check_add_packet_on_registered(split_line[1])
        add_packet = self.add_packets.get(split_line[1])

        # Третья компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[2])

        # # Подключаем абоненту доп. услугу
        sub.connect_add_packet(add_packet)

    def disconnect_additional_service_command(self, line: str) -> None:
        """Обрабатывает команду 'disconnect_additional_service'.

        Этапы обработки:
            1) Проверяем корректность входной строки:
                 > абонент с переданным номером
                   должен быть зарегистрирован;
                 > доп. услуга должна быть зарегистрирована;
                 > время должно быть задано корректным форматом.

            2) Отключаем пользователю доп. услугу.

        Формат строки 'команда + параметры':
                disconnect_additional_service number add_serv_name time

        time имеет формат: hh:mm:ss

        :param line: Строка с параметрами, разделенными пробелом.
        """
        # # Проверяем корректность принятой строки.
        #
        # Строка line должна состоять из 3-х компонент
        split_line = line.split()
        self.check_line_on_count_components('number add_serv_name time',
                                            split_line, 3)

        # Первая компонента должна описывать
        # номер абонента, который уже зарегистрирован в АТС.
        self.check_subscriber_on_registered(split_line[0])
        sub = self.subs.get(split_line[0])

        # Вторая компонента должна описывать
        # наименование доп. услуги, который уже зарегистрирован в АТС.
        self.check_add_packet_on_registered(split_line[1])
        add_packet_name = split_line[1]

        # Третья компонента должна описывать
        # корректное время.
        LogParser.time_line_to_datetime_obj(split_line[2])

        # # Отключаем у абонента доп. услугу
        sub.disconnect_add_packet(add_packet_name)
    # Конец методов обработки различных видов строк журнала.

    # Вспомогательные методы
    @staticmethod
    def days_count(first_datetime_obj: struct_time,
                   second_datetime_obj: struct_time) -> int:
        """Вычисляет количество прошедших дней.

        Порядок следования дней по возрастанию даты НЕ важен.
        :param first_datetime_obj: Первая дата.
        :param second_datetime_obj: Вторая дата.
        :return: Количество прошедших дней.
        """

        max_time = max(mktime(second_datetime_obj), mktime(first_datetime_obj))
        min_time = min(mktime(second_datetime_obj), mktime(first_datetime_obj))

        diff_in_seconds = max_time - min_time

        # Переводим секунды в дни с округлением вниз.
        diff_in_days = int(diff_in_seconds) // 86400

        return diff_in_days

    @staticmethod
    def date_line_to_datetime_obj(date_line: str) -> struct_time:
        """Преобразует строку описания даты в объект struct_time.
        :param date_line: Строка описания даты.
        :return: Объект даты в формате struct_time.
        """
        try:
            dt_obj = strptime('{} {}'.format(date_line, tzname[0]),
                              '%d.%m.%Y %Z')
        except ValueError:
            raise BadDatetimeLineException(
                'date_line_to_datetime_obj:\n'
                'Строка, описывающая дату, должна быть в следующем формате:\n'
                '%d.%m.%Y, где все эти величины '
                'представлены в формате модуля time.\n'
                '\n'
                'Исходная строка:\n{}'.format(date_line)
            )

        return dt_obj

    @staticmethod
    def time_line_to_datetime_obj(time_line: str) -> struct_time:
        """Преобразует строку описания даты в объект struct_time.
        :param time_line: Строка описания даты.
        :return: Объект даты в формате struct_time.
        """
        try:
            dt_obj = strptime('{} {}'.format(time_line, tzname[0]),
                              '%H:%M:%S %Z')
        except ValueError:
            raise BadDatetimeLineException(
                'line_line_to_datetime_obj:\n'
                'Строка, описывающая дату, должна быть в следующем формате:\n'
                '%H:%M:%S, где все эти величины '
                'представлены в формате модуля time.\n'
                '\n'
                'Исходная строка:\n{}'.format(time_line)
            )

        return dt_obj

    @staticmethod
    def call_length_in_minutes(start_datetime_obj: struct_time,
                               end_datetime_obj: struct_time) -> int:
        """Вычисляет длительность звонка.
        :param start_datetime_obj: Начальная дата_время.
        :param end_datetime_obj: Конечная дата_время.
        :return: Длительность звонка в минутах.
        """
        diff_in_seconds = mktime(end_datetime_obj) - mktime(start_datetime_obj)
        diff_in_seconds = int(diff_in_seconds)
        if diff_in_seconds < 0:
            raise AttributeError(
                'call_length_in_minutes:\n'
                'Дата начала разговора позже даты конца разгора.'
            )

        # Переводим секунды в минуты
        diff_in_minutes = diff_in_seconds // 60
        if diff_in_seconds % 60 > 0:
            diff_in_minutes += 1

        return diff_in_minutes

    @staticmethod
    def check_subscriber_on_activated(sub: Sub) -> None:
        """Проверяет, что абонент активирован.

        Если абонент заблокирован,
        переводим программу в критический режим.

        :param sub: Проверяемый абонент.
        """
        if not sub.is_activated:
            raise NotActivatedSubscriberException(
                '{}:\n'
                'Абонент с номером num={} заблокирован.'
                ''.format(inspect.stack()[1][3], sub.number)
            )

    def check_subscriber_on_registered(self, sub_number: str)-> None:
        """Проверяет существование абонента в АТС.

        :param sub_number: Номер абонента.
        """
        sub = self.subs.get(sub_number)
        if not sub:
            raise NotRegisteredSubscriberException(
                '{}:\n'
                'Абонент с номером number={}\n'
                'ещё не зарегистрирован в АТС.'
                ''.format(inspect.stack()[1][3], sub_number)
            )

    def check_rate_on_registered(self, rate_name: str)-> None:
        """Проверяет существование тарифа в АТС.

        :param rate_name: Наименование тарифа.
        """
        rate = self.rates.get(rate_name)

        if not rate:
            raise NotRegisteredRateException(
                '{}:\n'
                'Тариф с наименованием name=\'{}\'\n'
                'ещё не зарегистрирован в АТС.'

                # inspect.stack()[1][3] = имя функции,
                # вызвавшей текущую функцию
                ''.format(inspect.stack()[1][3], rate_name)
            )

    def check_add_packet_on_registered(self, add_packet_name: str)-> None:
        """Проверяет существование доп. услуги в АТС.

        :param add_packet_name: Наименование тарифа.
        """
        add_serv = self.add_packets.get(add_packet_name)
        if not add_serv:
            raise NotRegisteredAdditionalPacketException(
                'connect_additional_service_command:\n'
                'Доп. услуга с наименованием\nadd_serv_name={}\n'
                'ещё не зарегистрирована в АТС.'.format(add_packet_name)
            )

    @staticmethod
    def check_line_on_count_components(example_line: str,
                                       split_line: List[str],
                                       components_count: int) -> None:
        """Проверяет кол-во компонент в разбитой по пробелам строке.

        :param example_line: Строка-шаблон, которой должна
                             соответствовать последовательность
                             split_line
        :param split_line: Компоненты строки line
        :param components_count: Необходимое кол-во компонент.
        """
        if len(split_line) != components_count:
            raise BadDescriptionLineException(
                '{}:\n'
                'Строка line должна быть подобна следующей:\n'
                '{}.\n'
                '\n'
                'Исходная строка:\n{}'.format(inspect.stack()[1][3],
                                              example_line,
                                              ' '.join(split_line))
            )

    @staticmethod
    def get_decimal_number(num_in_str: str, attr_name: str) -> Decimal:
        try:
            return Decimal(num_in_str)
        except InvalidOperation:
            raise BadDecimalNumberException(
                '{}:\n'
                'Параметр {} должен быть '
                'рациональным числом (Decimal).\n'
                '\n'
                'Переданное число: {}'.format(inspect.stack()[1][3], attr_name,
                                              num_in_str)
            )
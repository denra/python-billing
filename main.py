import argparse

from help_parser import HelpParser
from log_parser import LogParser


def create_parser():
    arg_parser = argparse.ArgumentParser()

    # Обязательные параметры
    arg_parser.add_argument('path_to_rates_and_packets',
                            help='полный путь к файлу с описанием '
                                 'тарифов и пакетов.',
                            type=argparse.FileType())
    arg_parser.add_argument('path_to_log',
                            help='полный путь к файлу с описанием '
                                 'журнала.',
                            type=argparse.FileType())

    return arg_parser


def action_after_parsing(log_parser: LogParser):
    for sub in log_parser.subs.values():
        print('{0}: {1}'.format(sub.number, sub.balance))


def main():
    arg_parser = create_parser()
    namespace = arg_parser.parse_args()

    # Запоминаем пути к файлам с описанием
    path_to_rates_and_packets = namespace.path_to_rates_and_packets.name
    path_to_log = namespace.path_to_log.name

    # Читаем тарифы и пакеты
    h_parser = HelpParser()
    h_parser.handle_description_file(path_to_rates_and_packets)

    # Обрабатываем журнал операций АТС
    log_parser = LogParser(h_parser.data['rates'],
                           h_parser.data['add_packets'],
                           {})
    log_parser.handle_log_file(path_to_log)

    # Выводим инфу после парсинга
    action_after_parsing(log_parser)


if __name__ == '__main__':
    main()

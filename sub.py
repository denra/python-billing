from rate import Rate
from used_units import UsedUnits
from packet import Packet

from decimal import Decimal


class Sub:
    def __init__(self, number: str, rate: Rate) -> None:
        self.number = number

        self.rate = rate
        self.after_rate_pay_passed = 0

        # Содаем учет использования пакетов тарифа.
        self.used_units = {}
        for packet in rate.packets:
            self.used_units[packet.type] = UsedUnits()

        # Cоздаем хранилище доп. услуг
        self._add_packets = {}
        self._day_fee_for_add_packets = Decimal()

        self.is_activated = True
        self.balance = Decimal()

    def traffic_command(self, traffic_type: str, used_units: Decimal,
                        is_outbox: bool) -> None:
        """Для оплаты пакетов типа звонок, смс и т.д.

        1) Определить уложились ли мы
           в границы бесплатных единиц.
        2) Если нет, определить сколько мы должны заплатить.
        3) Увеличить кол-во использованных единиц.

        :param traffic_type: тип команды (звонок, смс)
        :param used_units: кол-во использованных единиц.
        :param is_outbox: исходящий ли трафик.
        """
        # Находим пакет типа traffic_type
        packet = self.rate.get_packet(traffic_type)

        # Выбираем параметры зависящие от направления трафика.
        if is_outbox:
            free = packet.free_outbox_units
            before = self.used_units[traffic_type].outbox_units
            extra_cost = packet.extra_outbox_cost
        else:
            free = packet.free_inbox_units
            before = self.used_units[traffic_type].inbox_units
            extra_cost = packet.extra_inbox_cost

        after = before + used_units

        # Находим сумму к оплате
        extra_traffic = self.get_extra_traffic(free, before, after)
        sum_to_pay = extra_traffic * extra_cost

        # Списываем сумму
        self.balance -= sum_to_pay

        # Запоминаем текущее кол-во трафика
        if is_outbox:
            self.used_units[traffic_type].outbox_units = after
        else:
            self.used_units[traffic_type].inbox_units = after

    @staticmethod
    def get_extra_traffic(free: Decimal, before: Decimal,
                          after: Decimal) -> Decimal:
        """Возвращает кол-во трафика сверх бесплатного.

        Возможно 3 случая:
        1) before <= after <= free
        2) before <= free < after
        2) free < before <= after

        :param free: Граница кол-во бесплатного трафика.
        :param before: Кол-во трафика перед добавлением.
        :param after: Кол-во трафика после добавления.
        :return: Кол-во трафика сверх бесплатного.
        """
        if not (before <= after):
            raise AttributeError()

        # 1)
        if after <= free:
            return Decimal()

        # 2)
        if before <= free < after:
            return after - free

        # 3)
        if free < before:
            return after - before

        raise AttributeError()

    def call_command(self, used_units: Decimal, is_outbox: bool) -> None:
        self.traffic_command('call', used_units, is_outbox)

    def sms_command(self, used_units: Decimal, is_outbox: bool) -> None:
        self.traffic_command('sms', used_units, is_outbox)

    def inet_command(self, used_units: Decimal, is_outbox: bool) -> None:
        self.traffic_command('inet', used_units, is_outbox)

    def change_rate(self, rate: Rate, change_cost: Decimal) -> None:
        """Изменяет тариф.
        1) Снимем плату за переключение тарифа.
        2) Переключим тариф.
        3) Обнуляем after_rate_pay_passed
        4) Обновим словарь used_units.

        :param rate: подключаемый тариф.
        :param change_cost: стоимость смены.
        """
        # 1)
        self.balance -= change_cost
        # 2)
        self.rate = rate
        # 3)
        self.after_rate_pay_passed = 0

        # 4)
        self.used_units.clear()
        for packet in rate.packets:
            self.used_units[packet.type] = UsedUnits()

    def add_money_command(self, amount: Decimal) -> None:
        if amount < 0:
            raise AttributeError('Нельзя добавлять отрицательное кол-во денег')
        self.balance += amount

    def activate(self) -> None:
        self.is_activated = True

    def deactivate(self) -> None:
        self.is_activated = False

    def connect_add_packet(self, add_packet: Packet) -> None:
        self._add_packets[add_packet.name] = add_packet
        self._day_fee_for_add_packets += add_packet.subscription_fee

        self.balance -= add_packet.subscription_fee

    def disconnect_add_packet(self, add_packet_name: str) -> None:
        add_packet = self._add_packets.get(add_packet_name)

        if add_packet:
            self._day_fee_for_add_packets -= add_packet.subscription_fee
            self._add_packets.pop(add_packet_name)

    def pay_add_packets(self) -> None:
        """Снимает ежедневную плату за доп. пакеты"""
        self.balance -= self._day_fee_for_add_packets

    def pay_rate(self) -> None:
        """Оплачивает тариф"""
        # Абонентская плата
        self.balance -= self.rate.subscription_fee

    def __get_money_for_extra_units(self) -> Decimal:
        d_null = Decimal()
        for_pay = d_null

        for rate_packet in self.rate.packets:
            used_units = self.used_units[rate_packet.type]

            extra_outbox = self.__get_extra_units(rate_packet.free_outbox_units,
                                                  used_units.outbox_units)

            extra_inbox = self.__get_extra_units(rate_packet.free_inbox_units,
                                                 used_units.inbox_units)

            # Сумма к оплате
            for_pay += (extra_outbox * rate_packet.extra_outbox_cost +
                       extra_inbox * rate_packet.extra_inbox_cost)

        return for_pay

    @staticmethod
    def __get_extra_units(free_units: Decimal, used_units: Decimal) -> Decimal:
        """Находит кол-во единиц сверх бесплатных.
        1) Если |free_units| == inf,
           то любое кол-во единиц - бесплатно.
        2) Если extra_outbox_units < 0,
           значит мы не вышли за предел бесплатных.

        :param free_units: Кол-во бесплатных единиц.
        :param used_units: Кол-во использованных единиц.
        :return: Кол-во единиц сверх бесплтаных.
        """

        # 1)
        if abs(free_units) == Decimal('inf'):
            return Decimal()

        # 2)
        extra_outbox_units = used_units - free_units
        if extra_outbox_units < Decimal():
            extra_outbox_units = Decimal()

        return extra_outbox_units

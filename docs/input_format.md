# Формат данных

## Журнал совершенных действий (log)

Все, что есть в логе, должно подчиняться правилам его формата,
иначе корректная интерпретация останется лишь мечтой.

### Правило № 1

Дата каждого дня должна присутствовать,
даже если в этот день не было совершено никаких операций.

Например:

>day 27.02.2017
>add_money +79089100346 100
>day 28.02.2017
>day 01.03.2017

### Правило № 2

Пусть start_day и end_day - начальный и конечный дни в логе
Пусть start_bound и end_bound - начальный и конечный границы

Если устанавливаются границы поиска, то они должны быть:

1. start_day <= start_bound <= end_day 
2. start_day <= end_bound <= end_day
3. start_bound <= end_bound

В противном случае, пограмма будет работать произвольно.

### Правило № 3

Явное лучше неявного, поэтому если абонента отключают за неуплату,
это должно быть явно зафиксировано в логе.

### Правило № 4

В одном файле описываются и тарифы, и пакеты.
Тариф обязательно должен содержать 3 пакета со следующими типами:

1. Тип: call. Звонок.
2. Тип: sms. Смс.
3. Тип: inet. Интернет.

#### Формат описания пакета:
packet <name> <type> <free_outbox_units> <free_inbox_units> <extra_outbox_cost> <extra_outbox_cost> <subscription_fee>, где

name -- наименование пакета
type -- тип пакета
free_outbox_units -- кол-во бесплатных исходящих единиц
free_inbox_units -- кол-во бесплатных входящих единиц
extra_outbox_cost -- стоимость 1 исходящей единицы сверх бесплатных
extra_inbox_cost -- стоимость 1 входящей единицы сверх бесплатных
subscription_fee -- абонентская плата за пакет

#### Формат описания тарифа:
rate <name> <packet_names> <sub_fee> <pay_period>, где
name -- наименование тарифа
packet_names -- наименования пакетов, входящих в основу тарифа
(формат: '[packet_name_1, packet_name_2, packet_name_3, ...]')
sub_fee -- абонентская плата за тариф
pay_period -- период оплаты тарифа.

### Правило № 5

Некоторые вещи о файле описания пакетов и тарифов:

1. Если в пакетах тарифа указан неизвестный пакет => вылетает исключение. 
from decimal import Decimal


class Packet:
    def __init__(self, name: str, packet_type: str, 
                 free_outbox_units: Decimal, free_inbox_units: Decimal,
                 extra_outbox_cost: Decimal, extra_inbox_cost: Decimal,
                 subscription_fee: Decimal) -> None:
        """
        name должно быть уникально, т.е. это идентификатор пакета. 
        """
        self._name = name
        self._type = packet_type

        # Если эти поля равны бесконечности,
        # то можно неограниченно пользоваться бесплатно.
        self._free_outbox_units = free_outbox_units
        self._free_inbox_units = free_inbox_units

        self._extra_outbox_cost = extra_outbox_cost
        self._extra_inbox_cost = extra_inbox_cost

        self._subscription_fee = subscription_fee

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    @property
    def free_outbox_units(self):
        return self._free_outbox_units

    @property
    def free_inbox_units(self):
        return self._free_inbox_units

    @property
    def extra_outbox_cost(self):
        return self._extra_outbox_cost

    @property
    def extra_inbox_cost(self):
        return self._extra_inbox_cost

    @property
    def subscription_fee(self):
        return self._subscription_fee

    def __eq__(self, other):
        """Пакеты равны <=> равны все их поля."""
        name_eq = self.name == other.name
        type_eq = self.type == other.type

        free_outbox_units_eq = (self.free_outbox_units ==
                                other.free_outbox_units)
        free_inbox_units_eq = (self.free_inbox_units ==
                               other.free_inbox_units)

        extra_outbox_cost_eq = (self.extra_outbox_cost ==
                                other.extra_outbox_cost)
        extra_inbox_cost_eq = (self.extra_inbox_cost ==
                                other.extra_inbox_cost)

        subscription_fee_eq = (self.subscription_fee ==
                                other.subscription_fee)

        return (name_eq and type_eq and free_inbox_units_eq and
                free_outbox_units_eq and extra_inbox_cost_eq and
                extra_outbox_cost_eq and subscription_fee_eq)

    def __hash__(self):
        return (hash(self.name) + hash(self.type) +
                hash(self.free_inbox_units) + hash(self.free_outbox_units) +
                hash(self.extra_inbox_cost) + hash(self.extra_outbox_cost) +
                hash(self.subscription_fee))
